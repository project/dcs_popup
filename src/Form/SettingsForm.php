<?php

namespace Drupal\dcs_popup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class SettingsForm.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'dcs_popup.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('dcs_popup.settings');
    $form['widget'] = [
      '#type' => 'select',
      '#title' => $this->t('Choose the Widget'),
      '#description' => $this->t('Choose widget for the Digital climate strike popup.'),
      '#options' => [
        'none' => $this->t('None'),
        'bottom' => $this->t('Bottom banner'),
        'page' => $this->t('Page popup'),
        ],
      '#size' => 1,
      '#default_value' => $config->get('widget'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('dcs_popup.settings')
      ->set('widget', $form_state->getValue('widget'))
      ->save();
  }

}
