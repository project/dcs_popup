CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module adds a "Digital Climate Strike" participation popup.

 * More info: https://digital.globalclimatestrike.net

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/dcs_popup

 * To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/dcs_popup

 * More info - https://digital.globalclimatestrike.net


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Digital Climate Strike (Popup) module as you would normally
   install a contributed Drupal module.
   Visit https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > System > Digital Climate Strike Popup Settings
    3. Choose the Widget for the Digital climate strike popup (Bottom banner or Page popup).
       Save configuration.


MAINTAINERS
-----------

 * Ilcho Vuchkov (vuil) - https://www.drupal.org/u/vuil

Supporting organizations:
 * FFW Agency - https://www.drupal.org/ffw-agency
